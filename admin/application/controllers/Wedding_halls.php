<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wedding_halls extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        
        date_default_timezone_set("Asia/Kolkata");
        
        $this->load->model('Wedding_halls_model');
        if (!$this->session->userdata('logged_in_admin')) {
            redirect(base_url());
        }
    }
    
    function view_hall(){
        $settings        = get_settings();
        $header['title'] = $settings->title . " | View Wedding Halls";
        $this->load->view('Templates/header', $header);
        $template['data']  = $this->Wedding_halls_model->view_hall();
        $this->load->view('halls/view_hall', $template);
        $this->load->view('Templates/footer');
    }
    
    function add_hall(){
        $settings        = get_settings();
        $header['title'] = $settings->title . " | Add Wedding Hall";
        $this->load->view('Templates/header', $header);
        $this->load->view('halls/add_hall');
        $this->load->view('Templates/footer');
        
        if ($_POST) {
            
            $data = $_POST; 

            if(isset($_FILES['image_1'])) {  
                $config = set_upload_logo('assets/uploads/wedding_halls');
                $this->load->library('upload');
                
                $new_name = time()."_".$_FILES["image_1"]['name'];
                $config['file_name'] = $new_name;

                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image_1')) {
                    unset($data['image_1']);
                }
                else {
                    $upload_data = $this->upload->data();
                    //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                    $data['image_1'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                }
            }
            if(isset($_FILES['image_2'])) {  
                $config = set_upload_logo('assets/uploads/wedding_halls');
                $this->load->library('upload');
                
                $new_name = time()."_".$_FILES["image_2"]['name'];
                $config['file_name'] = $new_name;

                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image_2')) {
                    unset($data['image_2']);
                }
                else {
                    $upload_data = $this->upload->data();
                    //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                    $data['image_2'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                }
            }
            if(isset($_FILES['image_3'])) {  
                $config = set_upload_logo('assets/uploads/wedding_halls');
                $this->load->library('upload');
                
                $new_name = time()."_".$_FILES["image_3"]['name'];
                $config['file_name'] = $new_name;

                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image_3')) {
                    unset($data['image_3']);
                }
                else {
                    $upload_data = $this->upload->data();
                    //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                    $data['image_3'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                }
            }
            if(isset($_FILES['image_4'])) {  
                $config = set_upload_logo('assets/uploads/wedding_halls');
                $this->load->library('upload');
                
                $new_name = time()."_".$_FILES["image_4"]['name'];
                $config['file_name'] = $new_name;

                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image_4')) {
                    unset($data['image_4']);
                }
                else {
                    $upload_data = $this->upload->data();
                    //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                    $data['image_4'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                }
            }
            if(isset($_FILES['image_5'])) {  
                $config = set_upload_logo('assets/uploads/wedding_halls');
                $this->load->library('upload');
                
                $new_name = time()."_".$_FILES["image_5"]['name'];
                $config['file_name'] = $new_name;

                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image_5')) {
                    unset($data['image_5']);
                }
                else {
                    $upload_data = $this->upload->data();
                    //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                    $data['image_5'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                }
            }
            if(isset($_FILES['image_6'])) {  
                $config = set_upload_logo('assets/uploads/wedding_halls');
                $this->load->library('upload');
                
                $new_name = time()."_".$_FILES["image_6"]['name'];
                $config['file_name'] = $new_name;

                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image_6')) {
                    unset($data['image_6']);
                }
                else {
                    $upload_data = $this->upload->data();
                    //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                    $data['image_6'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                }
            }
            if(isset($_FILES['image_7'])) {  
                $config = set_upload_logo('assets/uploads/wedding_halls');
                $this->load->library('upload');
                
                $new_name = time()."_".$_FILES["image_7"]['name'];
                $config['file_name'] = $new_name;

                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image_7')) {
                    unset($data['image_7']);
                }
                else {
                    $upload_data = $this->upload->data();
                    //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                    $data['image_7'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                }
            }
            if(isset($_FILES['image_8'])) {  
                $config = set_upload_logo('assets/uploads/wedding_halls');
                $this->load->library('upload');
                
                $new_name = time()."_".$_FILES["image_8"]['name'];
                $config['file_name'] = $new_name;

                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image_8')) {
                    unset($data['image_8']);
                }
                else {
                    $upload_data = $this->upload->data();
                    //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                    $data['image_8'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                }
            }
            if(isset($_FILES['image_9'])) {  
                $config = set_upload_logo('assets/uploads/wedding_halls');
                $this->load->library('upload');
                
                $new_name = time()."_".$_FILES["image_9"]['name'];
                $config['file_name'] = $new_name;

                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image_9')) {
                    unset($data['image_9']);
                }
                else {
                    $upload_data = $this->upload->data();
                    //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                    $data['image_9'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                }
            }
            if(isset($_FILES['image_10'])) {  
                $config = set_upload_logo('assets/uploads/wedding_halls');
                $this->load->library('upload');
                
                $new_name = time()."_".$_FILES["image_10"]['name'];
                $config['file_name'] = $new_name;

                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image_10')) {
                    unset($data['image_10']);
                }
                else {
                    $upload_data = $this->upload->data();
                    //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                    $data['image_10'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                }
            }

            $data['created_date'] = date("Y-m-d H:i:s");
            $data['modified_date'] = date("Y-m-d H:i:s");
            $result = $this->Wedding_halls_model->add_hall($data);

            if ($result) {
                $this->session->set_flashdata('message', array(
                    'message' => 'Success',
                    'class' => 'success'
                ));
            } else {
                $this->session->set_flashdata('message', array(
                    'message' => 'Error',
                    'class' => 'error'
                ));
            }
            redirect(base_url() . 'Wedding_halls/view_hall');
        }
    }
    
    function edit_hall(){
        $settings = get_settings();
        $header['title'] = $settings->title . " | Edit Wedding Hall";
        $this->load->view('Templates/header', $header);
        
        $id = $this->uri->segment(3);
        $template['data']  = $this->Wedding_halls_model->editget_hall_id($id);
        $this->load->view('halls/edit_hall', $template);
        $this->load->view('Templates/footer');

        if (!empty($template['data'])) {
            if ($_POST) {

                $data = $_POST;

                if(isset($_FILES['image_1'])) {  
                    $config = set_upload_logo('assets/uploads/wedding_halls');
                    $this->load->library('upload');
                    
                    $new_name = time()."_".$_FILES["image_1"]['name'];
                    $config['file_name'] = $new_name;

                    $this->upload->initialize($config);

                    if ( ! $this->upload->do_upload('image_1')) {
                        unset($data['image_1']);
                    }
                    else {
                        $upload_data = $this->upload->data();
                        //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                        $data['image_1'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                    }
                }
                if(isset($_FILES['image_2'])) {  
                    $config = set_upload_logo('assets/uploads/wedding_halls');
                    $this->load->library('upload');
                    
                    $new_name = time()."_".$_FILES["image_2"]['name'];
                    $config['file_name'] = $new_name;

                    $this->upload->initialize($config);

                    if ( ! $this->upload->do_upload('image_2')) {
                        unset($data['image_2']);
                    }
                    else {
                        $upload_data = $this->upload->data();
                        //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                        $data['image_2'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                    }
                }
                if(isset($_FILES['image_3'])) {  
                    $config = set_upload_logo('assets/uploads/wedding_halls');
                    $this->load->library('upload');
                    
                    $new_name = time()."_".$_FILES["image_3"]['name'];
                    $config['file_name'] = $new_name;

                    $this->upload->initialize($config);

                    if ( ! $this->upload->do_upload('image_3')) {
                        unset($data['image_3']);
                    }
                    else {
                        $upload_data = $this->upload->data();
                        //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                        $data['image_3'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                    }
                }
                if(isset($_FILES['image_4'])) {  
                    $config = set_upload_logo('assets/uploads/wedding_halls');
                    $this->load->library('upload');
                    
                    $new_name = time()."_".$_FILES["image_4"]['name'];
                    $config['file_name'] = $new_name;

                    $this->upload->initialize($config);

                    if ( ! $this->upload->do_upload('image_4')) {
                        unset($data['image_4']);
                    }
                    else {
                        $upload_data = $this->upload->data();
                        //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                        $data['image_4'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                    }
                }
                if(isset($_FILES['image_5'])) {  
                    $config = set_upload_logo('assets/uploads/wedding_halls');
                    $this->load->library('upload');
                    
                    $new_name = time()."_".$_FILES["image_5"]['name'];
                    $config['file_name'] = $new_name;

                    $this->upload->initialize($config);

                    if ( ! $this->upload->do_upload('image_5')) {
                        unset($data['image_5']);
                    }
                    else {
                        $upload_data = $this->upload->data();
                        //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                        $data['image_5'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                    }
                }
                if(isset($_FILES['image_6'])) {  
                    $config = set_upload_logo('assets/uploads/wedding_halls');
                    $this->load->library('upload');
                    
                    $new_name = time()."_".$_FILES["image_6"]['name'];
                    $config['file_name'] = $new_name;

                    $this->upload->initialize($config);

                    if ( ! $this->upload->do_upload('image_6')) {
                        unset($data['image_6']);
                    }
                    else {
                        $upload_data = $this->upload->data();
                        //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                        $data['image_6'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                    }
                }
                if(isset($_FILES['image_7'])) {  
                    $config = set_upload_logo('assets/uploads/wedding_halls');
                    $this->load->library('upload');
                    
                    $new_name = time()."_".$_FILES["image_7"]['name'];
                    $config['file_name'] = $new_name;

                    $this->upload->initialize($config);

                    if ( ! $this->upload->do_upload('image_7')) {
                        unset($data['image_7']);
                    }
                    else {
                        $upload_data = $this->upload->data();
                        //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                        $data['image_7'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                    }
                }
                if(isset($_FILES['image_8'])) {  
                    $config = set_upload_logo('assets/uploads/wedding_halls');
                    $this->load->library('upload');
                    
                    $new_name = time()."_".$_FILES["image_8"]['name'];
                    $config['file_name'] = $new_name;

                    $this->upload->initialize($config);

                    if ( ! $this->upload->do_upload('image_8')) {
                        unset($data['image_8']);
                    }
                    else {
                        $upload_data = $this->upload->data();
                        //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                        $data['image_8'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                    }
                }
                if(isset($_FILES['image_9'])) {  
                    $config = set_upload_logo('assets/uploads/wedding_halls');
                    $this->load->library('upload');
                    
                    $new_name = time()."_".$_FILES["image_9"]['name'];
                    $config['file_name'] = $new_name;

                    $this->upload->initialize($config);

                    if ( ! $this->upload->do_upload('image_9')) {
                        unset($data['image_9']);
                    }
                    else {
                        $upload_data = $this->upload->data();
                        //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                        $data['image_9'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                    }
                }
                if(isset($_FILES['image_10'])) {  
                    $config = set_upload_logo('assets/uploads/wedding_halls');
                    $this->load->library('upload');
                    
                    $new_name = time()."_".$_FILES["image_10"]['name'];
                    $config['file_name'] = $new_name;

                    $this->upload->initialize($config);

                    if ( ! $this->upload->do_upload('image_10')) {
                        unset($data['image_10']);
                    }
                    else {
                        $upload_data = $this->upload->data();
                        //$data['logo'] = $config['upload_path']."/".$upload_data['file_name'];
                        $data['image_10'] = base_url().$config['upload_path']."/".$upload_data['file_name'];
                    }
                }

                $data['modified_date'] = date("Y-m-d H:i:s");
                // echo "<pre>";
                // print_r($data);die();

                $result = $this->Wedding_halls_model->edit_hall($data, $id);
                if ($result) {
                    $this->session->set_flashdata('message', array(
                        'message' => "Wedding hall details updated successfully.",
                        'class' => 'success'
                    ));
                }
                redirect(base_url() . 'Wedding_halls/view_hall');
            }
        } else {
            $this->session->set_flashdata('message', array(
                'message' => "You don't have permission to access.",
                'class' => 'danger'
            ));
            redirect(base_url() . 'Wedding_halls/view_hall');
        }  
    }
    
    function delete_hall(){
        $id     = $this->uri->segment(3);
        $result = $this->Wedding_halls_model->delete_hall($id);
        $this->session->set_flashdata('message', array(
            'message' => 'Deleted Successfully',
            'class' => 'success'
        ));
        redirect(base_url() . 'Wedding_halls/view_hall');
    }
}
?>