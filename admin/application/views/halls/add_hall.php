<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Add Hall Details</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class=""></i>Home</a></li>
      <li><a href="<?php echo base_url(); ?>Wedding_halls/view_hall">Wedding Halls</a></li>
      <li class="active">Add Hall Details</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-warning">
          <div class="box-header with-border"><h3 class="box-title">Add Hall Details</h3></div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="" method="post" data-parsley-validate class="validate" enctype="multipart/form-data">
            <div class="box-body">
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group has-feedback">
                    <label for="name">Hall Name</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"	
                    data-parsley-minlength="2" data-parsley-maxlength="100" data-parsley-pattern="^[a-zA-Z0-9\ \-,/\']*$" required="" name="name"  placeholder="Hall Name">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 

                  <div class="form-group has-feedback">
                    <label for="address">Address</label>
                    <textarea type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" rows="4" data-parsley-maxlength="500" data-parsley-pattern="^[a-zA-Z0-9\ \-,./\']*$" required="" name="address"  placeholder="Address"></textarea>
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>  

                  <div class="form-group has-feedback">
                    <label for="city">City</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="city"  placeholder="City">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 

                  <div class="form-group has-feedback">
                    <label for="state">State</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="state"  placeholder="State">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 

                  <div class="form-group has-feedback">
                    <label for="country">Country</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="country"  placeholder="Country">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 
                </div>
                <div class="col-md-6">

                  <div class="form-group has-feedback">
                    <label for="contact_person">Contact Person Name</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="contact_person"  placeholder="Contact Person Name">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 
                  <div class="form-group has-feedback">
                    <label for="contact_email">Contact Person Email</label>
                    <input type="email" class="form-control required" data-parsley-trigger="change"  
                    required="" name="contact_email"  placeholder="Contact Person Email">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 

                  <div class="form-group has-feedback">
                    <label for="contact_no">Contact Person Phone</label>
                    <input type="digit" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$" required="" name="contact_no"  placeholder="Contact Person Phone">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="description">Description</label>
                    <textarea type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" rows="4" data-parsley-maxlength="2000" data-parsley-pattern="^[a-zA-Z0-9\ \-,.&/\']*$" required="" name="description"  placeholder="Description"></textarea>
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 
                </div>
              </div>

              <div class="col-md-12">
                <div class="col-md-6">
                  <br><br><label>Features</label><br><br>
                  <div class="form-group has-feedback">
                    <label for="hall_seating">Hall Seating</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="hall_seating"  placeholder="Hall Seating">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="hall_floating">Hall Floating</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="hall_floating"  placeholder="Hall Floating">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="dining_seating">Dining Seating</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="dining_seating"  placeholder="Dining Seating">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="dining_hall_floor">Dining Hall Floor</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[a-zA-Z0-9\  \/]+$" required="" name="dining_hall_floor"  placeholder="Dining Hall Floor">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="car_parking">Car Parking</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="car_parking"  placeholder="Car Parking">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <br><br><br><br>
                  <div class="form-group has-feedback">
                    <label for="no_of_rooms">No Of Rooms</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="1" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="no_of_rooms"  placeholder="No Of Rooms">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="no_of_ac_rooms">No Of A/C Rooms</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="1" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="no_of_ac_rooms"  placeholder="No Of A/C Rooms">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="hall_sq_ft">Hall Square Ft</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="hall_sq_ft"  placeholder="Hall Square Ft">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="hall_floor">Hall Floor</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[a-zA-Z0-9\  \/]+$" required="" name="hall_floor"  placeholder="Hall Floor">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="bike_parking">Bike Parking</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="bike_parking"  placeholder="Bike Parking">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 
                </div>
              </div>

              <div class="col-md-12">
                <div class="col-md-6">
                  <br><br><label>Hall Images</label><br><br>
                  <div class="form-group">
                    <label  class="intrate">Image 1</label>
                    <input name="image_1" class="" accept="image/*" type="file">
                  </div>

                  <div class="form-group">
                    <label  class="intrate">Image 2</label>
                    <input name="image_2" class="" accept="image/*" type="file">
                  </div>

                  <div class="form-group">
                    <label  class="intrate">Image 3</label>
                    <input name="image_3" class="" accept="image/*" type="file">
                  </div> 

                  <div class="form-group">
                    <label  class="intrate">Image 4</label>
                    <input name="image_4" class="" accept="image/*" type="file">
                  </div>

                  <div class="form-group">
                    <label  class="intrate">Image 5</label>
                    <input name="image_5" class="" accept="image/*" type="file">
                  </div>
                </div>
                <div class="col-md-6">
                  <br><br><br><br>
                  <div class="form-group">
                    <label  class="intrate">Image 6</label>
                    <input name="image_6" class="" accept="image/*" type="file">
                  </div>

                  <div class="form-group">
                    <label  class="intrate">Image 7</label>
                    <input name="image_7" class="" accept="image/*" type="file">
                  </div>
                  
                  <div class="form-group">
                    <label  class="intrate">Image 8</label>
                    <input name="image_8" class="" accept="image/*" type="file">
                  </div> 
                  
                  <div class="form-group">
                    <label  class="intrate">Image 9</label>
                    <input name="image_9" class="" accept="image/*" type="file">
                  </div>
                  
                  <div class="form-group">
                    <label  class="intrate">Image 10</label>
                    <input name="image_10" class="" accept="image/*" type="file">
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="col-md-6">
                  <br><br><label>Others</label><br><br>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="lift_for_hall">
                    <label for="lift_for_hall">Lift for Hall Available</label>
                    <input type='hidden' value='0' name='lift_for_hall'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="lift_for_dining_hall">
                    <label for="lift_for_dining_hall">Lift for Dining Hall Available</label>
                    <input type='hidden' value='0' name='lift_for_dining_hall'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="hall_ac">
                    <label for="hall_ac">Hall A/C Available</label>
                    <input type='hidden' value='0' name='hall_ac'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="dining_ac">
                    <label for="dining_ac">Dining A/C Available</label>
                    <input type='hidden' value='0' name='dining_ac'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="common_ac">
                    <label for="common_ac">Common Area A/C Available</label>
                    <input type='hidden' value='0' name='common_ac'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="generator_bckp">
                    <label for="generator_bckp">Generator Backup Available</label>
                    <input type='hidden' value='0' name='generator_bckp'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="safe_avilable">
                    <label for="safe_avilable">Safe Available</label>
                    <input type='hidden' value='0' name='safe_avilable'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="outdoor_catering">
                    <label for="outdoor_catering">Outdoor Catering Allowed</label>
                    <input type='hidden' value='0' name='outdoor_catering'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="non_veg_cooking_allowed">
                    <label for="non_veg_cooking_allowed">Non-veg Cooking Allowed</label>
                    <input type='hidden' value='0' name='non_veg_cooking_allowed'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="non_veg_service_allowed">
                    <label for="non_veg_service_allowed">Non-veg Service Allowed</label>
                    <input type='hidden' value='0' name='non_veg_service_allowed'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="kitchen_vessels_available">
                    <label for="kitchen_vessels_available">Kitchen Vessels Available</label>
                    <input type='hidden' value='0' name='kitchen_vessels_available'>
                  </div>
                </div>
                <div class="col-md-6">
                  <br><br><br><br>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="buffet_crockery_available">
                    <label for="buffet_crockery_available">Buffet Crockery Available</label>
                    <input type='hidden' value='0' name='buffet_crockery_available'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="cooking_fuel_provided">
                    <label for="cooking_fuel_provided">Cooking Fuel Provided</label>
                    <input type='hidden' value='0' name='cooking_fuel_provided'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="valet_parking">
                    <label for="valet_parking">Valet Parking Available</label>
                    <input type='hidden' value='0' name='valet_parking'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="handicap_ramp">
                    <label for="handicap_ramp">Handicap Ramp Available</label>
                    <input type='hidden' value='0' name='handicap_ramp'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="stage_available">
                    <label for="stage_available">Stage Available</label>
                    <input type='hidden' value='0' name='stage_available'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="outside_vendor_allowed">
                    <label for="outside_vendor_allowed">Outside Vendor Allowed</label>
                    <input type='hidden' value='0' name='outside_vendor_allowed'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="outside_entertainment_allowed">
                    <label for="outside_entertainment_allowed">Outside Entertainment Allowed</label>
                    <input type='hidden' value='0' name='outside_entertainment_allowed'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="ritual">
                    <label for="ritual">Ritual Allowed (Homam / Havan / Fire Ritual)</label>
                    <input type='hidden' value='0' name='ritual'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="on_main_road">
                    <label for="on_main_road">Approach On Main Road</label>
                    <input type='hidden' value='0' name='on_main_road'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="residential_area">
                    <label for="residential_area">Approach Residential Area</label>
                    <input type='hidden' value='0' name='residential_area'>
                  </div>
                </div>
              </div>
            </div> 
            <div class="box-footer center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>             
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->
  </section>
<!-- /.content -->
</div>
<script type="text/javascript">
  $('input[type="checkbox"]').change(function() {
    var check = $(this).prop('checked');
    console.log(check);
    if (check==true)
      $(this).closest(".form-group").find('input[type="hidden"]').val('1');
    else 
      $(this).closest(".form-group").find('input[type="hidden"]').val('0');
  });
</script>