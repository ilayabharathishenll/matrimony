<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Edit Hall Details</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class=""></i>Home</a></li>
      <li><a href="<?php echo base_url(); ?>Wedding_halls/view_hall">Wedding Halls</a></li>
      <li class="active">Edit Hall Details</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-warning">
          <div class="box-header with-border"><h3 class="box-title">Edit Hall Details</h3></div>
          <!-- /.box-header -->
          <!-- form start -->
          <style type="text/css">img {margin-top: 10px;}</style>
          <form role="form" action="" method="post" data-parsley-validate class="validate" enctype="multipart/form-data">
            <div class="box-body">
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group has-feedback">
                    <label for="name">Hall Name</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="100" data-parsley-pattern="^[a-zA-Z0-9\ \-,/\']*$" required="" name="name"  placeholder="Hall Name" value="<?php echo $data->name;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 

                  <div class="form-group has-feedback">
                    <label for="address">Address</label>
                    <textarea type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" rows="4" data-parsley-maxlength="500" data-parsley-pattern="^[a-zA-Z0-9\ \-,./\']*$" required="" name="address"  placeholder="Address"><?php echo $data->address;?></textarea>
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>  

                  <div class="form-group has-feedback">
                    <label for="city">City</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="city"  placeholder="City" value="<?php echo $data->city;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 

                  <div class="form-group has-feedback">
                    <label for="state">State</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="state"  placeholder="State" value="<?php echo $data->state;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 

                  <div class="form-group has-feedback">
                    <label for="country">Country</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="country"  placeholder="Country" value="<?php echo $data->country;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 
                </div>
                <div class="col-md-6">

                  <div class="form-group has-feedback">
                    <label for="contact_person">Contact Person Name</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[a-zA-Z\  \/]+$" required="" name="contact_person"  placeholder="Contact Person Name" value="<?php echo $data->contact_person;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 
                  <div class="form-group has-feedback">
                    <label for="contact_email">Contact Person Email</label>
                    <input type="email" class="form-control required" data-parsley-trigger="change"  
                    required="" name="contact_email"  placeholder="Contact Person Email"  value="<?php echo $data->contact_email;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 

                  <div class="form-group has-feedback">
                    <label for="contact_no">Contact Person Phone</label>
                    <input type="digit" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="15" data-parsley-pattern="^[0-9\  \/]+$" required="" name="contact_no"  placeholder="Contact Person Phone"  value="<?php echo $data->contact_no;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="description">Description</label>
                    <textarea type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" rows="4" data-parsley-maxlength="2000" data-parsley-pattern="^[a-zA-Z0-9\ \-,.&/\']*$" required="" name="description"  placeholder="Description"><?php echo $data->description;?></textarea>
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 
                </div>
              </div>

              <div class="col-md-12">
                <div class="col-md-6">
                  <br><br><label>Features</label><br><br>
                  <div class="form-group has-feedback">
                    <label for="hall_seating">Hall Seating</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="hall_seating"  placeholder="Hall Seating" value="<?php echo $data->hall_seating;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="hall_floating">Hall Floating</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="hall_floating"  placeholder="Hall Floating" value="<?php echo $data->hall_floating;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="dining_seating">Dining Seating</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="dining_seating"  placeholder="Dining Seating" value="<?php echo $data->dining_seating;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="dining_hall_floor">Dining Hall Floor</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[a-zA-Z0-9\  \/]+$" required="" name="dining_hall_floor"  placeholder="Dining Hall Floor" value="<?php echo $data->dining_hall_floor;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="car_parking">Car Parking</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="car_parking"  placeholder="Car Parking" value="<?php echo $data->car_parking;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <br><br><br><br>
                  <div class="form-group has-feedback">
                    <label for="no_of_rooms">No Of Rooms</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="1" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="no_of_rooms"  placeholder="No Of Rooms" value="<?php echo $data->no_of_rooms;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="no_of_ac_rooms">No Of A/C Rooms</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="1" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="no_of_ac_rooms"  placeholder="No Of A/C Rooms" value="<?php echo $data->no_of_ac_rooms;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="hall_sq_ft">Hall Square Ft</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="hall_sq_ft"  placeholder="Hall Square Ft" value="<?php echo $data->hall_sq_ft;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="hall_floor">Hall Floor</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[a-zA-Z0-9\  \/]+$" required="" name="hall_floor"  placeholder="Hall Floor" value="<?php echo $data->hall_floor;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">
                    <label for="bike_parking">Bike Parking</label>
                    <input type="text" class="form-control required" data-parsley-trigger="change"  
                    data-parsley-minlength="2" data-parsley-maxlength="50" data-parsley-pattern="^[0-9\  \/]+$" required="" name="bike_parking"  placeholder="Bike Parking" value="<?php echo $data->bike_parking;?>">
                    <span class="glyphicon  form-control-feedback"></span>
                  </div> 
                </div>
              </div>

              <div class="col-md-12">
                <div class="col-md-6">
                  <br><br><label>Hall Images</label><br><br>
                  <div class="form-group">
                    <label  class="intrate">Image 1</label>
                    <input name="image_1" class="" accept="image/*" type="file">
                    <?php if($data->image_1!=""){ ?>
                      <img src="<?php echo $data->image_1; ?>" width="100px" height="50px" alt="Picture Not Found"/>
                    <?php } ?>
                  </div>

                  <div class="form-group">
                    <label  class="intrate">Image 2</label>
                    <input name="image_2" class="" accept="image/*" type="file">
                    <?php if($data->image_2!=""){ ?>
                      <img src="<?php echo $data->image_2; ?>" width="100px" height="50px" alt="Picture Not Found"/>
                    <?php } ?>
                  </div>

                  <div class="form-group">
                    <label  class="intrate">Image 3</label>
                    <input name="image_3" class="" accept="image/*" type="file">
                    <?php if($data->image_3!=""){ ?>
                      <img src="<?php echo $data->image_3; ?>" width="100px" height="50px" alt="Picture Not Found"/>
                    <?php } ?>
                  </div> 

                  <div class="form-group">
                    <label  class="intrate">Image 4</label>
                    <input name="image_4" class="" accept="image/*" type="file">
                    <?php if($data->image_4!=""){ ?>
                      <img src="<?php echo $data->image_4; ?>" width="100px" height="50px" alt="Picture Not Found"/>
                    <?php } ?>
                  </div>

                  <div class="form-group">
                    <label  class="intrate">Image 5</label>
                    <input name="image_5" class="" accept="image/*" type="file">
                    <?php if($data->image_5!=""){ ?>
                      <img src="<?php echo $data->image_5; ?>" width="100px" height="50px" alt="Picture Not Found"/>
                    <?php } ?>
                  </div>
                </div>
                <div class="col-md-6">
                  <br><br><br><br>
                  <div class="form-group">
                    <label  class="intrate">Image 6</label>
                    <input name="image_6" class="" accept="image/*" type="file">
                    <?php if($data->image_6!=""){ ?>
                      <img src="<?php echo $data->image_6; ?>" width="100px" height="50px" alt="Picture Not Found"/>
                    <?php } ?>
                  </div>

                  <div class="form-group">
                    <label  class="intrate">Image 7</label>
                    <input name="image_7" class="" accept="image/*" type="file">
                    <?php if($data->image_7!=""){ ?>
                      <img src="<?php echo $data->image_7; ?>" width="100px" height="50px" alt="Picture Not Found"/>
                    <?php } ?>
                  </div>
                  
                  <div class="form-group">
                    <label  class="intrate">Image 8</label>
                    <input name="image_8" class="" accept="image/*" type="file">
                    <?php if($data->image_8!=""){ ?>
                      <img src="<?php echo $data->image_8; ?>" width="100px" height="50px" alt="Picture Not Found"/>
                    <?php } ?>
                  </div> 
                  
                  <div class="form-group">
                    <label  class="intrate">Image 9</label>
                    <input name="image_9" class="" accept="image/*" type="file">
                    <?php if($data->image_9!=""){ ?>
                      <img src="<?php echo $data->image_9; ?>" width="100px" height="50px" alt="Picture Not Found"/>
                    <?php } ?>
                  </div>
                  
                  <div class="form-group">
                    <label  class="intrate">Image 10</label>
                    <input name="image_10" class="" accept="image/*" type="file">
                    <?php if($data->image_10!=""){ ?>
                      <img src="<?php echo $data->image_10; ?>" width="100px" height="50px" alt="Picture Not Found"/>
                    <?php } ?>
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="col-md-6">
                  <br><br><label>Others</label><br><br>
                  <?php 
                    if($data->lift_for_hall=='1'){
                      $lift_for_hall_attr = 'checked="checked"';
                      $lift_for_hall_val = '1';
                    } else {
                      $lift_for_hall_val = '0';
                      $lift_for_hall_attr = '';
                    }
                    if($data->lift_for_dining_hall=='1'){
                      $lift_for_dining_hall_attr = 'checked="checked"';
                      $lift_for_dining_hall_val = '1';
                    } else {
                      $lift_for_dining_hall_val = '0';
                      $lift_for_dining_hall_attr = '';
                    }
                    if($data->hall_ac=='1'){
                      $hall_ac_attr = 'checked="checked"';
                      $hall_ac_val = '1';
                    } else {
                      $hall_ac_val = '0';
                      $hall_ac_attr = '';
                    }
                    if($data->dining_ac=='1'){
                      $dining_ac_attr = 'checked="checked"';
                      $dining_ac_val = '1';
                    } else {
                      $dining_ac_val = '0';
                      $dining_ac_attr = '';
                    }
                    if($data->common_ac=='1'){
                      $common_ac_attr = 'checked="checked"';
                      $common_ac_val = '1';
                    } else {
                      $common_ac_val = '0';
                      $common_ac_attr = '';
                    }
                    if($data->generator_bckp=='1'){
                      $generator_bckp_attr = 'checked="checked"';
                      $generator_bckp_val = '1';
                    } else {
                      $generator_bckp_val = '0';
                      $generator_bckp_attr = '';
                    }
                    if($data->safe_avilable=='1'){
                      $safe_avilable_attr = 'checked="checked"';
                      $safe_avilable_val = '1';
                    } else {
                      $safe_avilable_val = '0';
                      $safe_avilable_attr = '';
                    }
                    if($data->outdoor_catering=='1'){
                      $outdoor_catering_attr = 'checked="checked"';
                      $outdoor_catering_val = '1';
                    } else {
                      $outdoor_catering_val = '0';
                      $outdoor_catering_attr = '';
                    }
                    if($data->non_veg_cooking_allowed=='1'){
                      $non_veg_cooking_allowed_attr = 'checked="checked"';
                      $non_veg_cooking_allowed_val = '1';
                    } else {
                      $non_veg_cooking_allowed_val = '0';
                      $non_veg_cooking_allowed_attr = '';
                    }
                    if($data->non_veg_service_allowed=='1'){
                      $non_veg_service_allowed_attr = 'checked="checked"';
                      $non_veg_service_allowed_val = '1';
                    } else {
                      $non_veg_service_allowed_val = '0';
                      $non_veg_service_allowed_attr = '';
                    }
                    if($data->kitchen_vessels_available=='1'){
                      $kitchen_vessels_available_attr = 'checked="checked"';
                      $kitchen_vessels_available_val = '1';
                    } else {
                      $kitchen_vessels_available_val = '0';
                      $kitchen_vessels_available_attr = '';
                    }
                    if($data->buffet_crockery_available=='1'){
                      $buffet_crockery_available_attr = 'checked="checked"';
                      $buffet_crockery_available_val = '1';
                    } else {
                      $buffet_crockery_available_val = '0';
                      $buffet_crockery_available_attr = '';
                    }
                    if($data->cooking_fuel_provided=='1'){
                      $cooking_fuel_provided_attr = 'checked="checked"';
                      $cooking_fuel_provided_val = '1';
                    } else {
                      $cooking_fuel_provided_val = '0';
                      $cooking_fuel_provided_attr = '';
                    }
                    if($data->valet_parking=='1'){
                      $valet_parking_attr = 'checked="checked"';
                      $valet_parking_val = '1';
                    } else {
                      $valet_parking_val = '0';
                      $valet_parking_attr = '';
                    }
                    if($data->handicap_ramp=='1'){
                      $handicap_ramp_attr = 'checked="checked"';
                      $handicap_ramp_val = '1';
                    } else {
                      $handicap_ramp_val = '0';
                      $handicap_ramp_attr = '';
                    }
                    if($data->stage_available=='1'){
                      $stage_available_attr = 'checked="checked"';
                      $stage_available_val = '1';
                    } else {
                      $stage_available_val = '0';
                      $stage_available_attr = '';
                    }
                    if($data->outside_vendor_allowed=='1'){
                      $outside_vendor_allowed_attr = 'checked="checked"';
                      $outside_vendor_allowed_val = '1';
                    } else {
                      $outside_vendor_allowed_val = '0';
                      $outside_vendor_allowed_attr = '';
                    }
                    if($data->outside_entertainment_allowed=='1'){
                      $outside_entertainment_allowed_attr = 'checked="checked"';
                      $outside_entertainment_allowed_val = '1';
                    } else {
                      $outside_entertainment_allowed_val = '0';
                      $outside_entertainment_allowed_attr = '';
                    }
                    if($data->ritual=='1'){
                      $ritual_attr = 'checked="checked"';
                      $ritual_val = '1';
                    } else {
                      $ritual_val = '0';
                      $ritual_attr = '';
                    }
                    if($data->on_main_road=='1'){
                      $on_main_road_attr = 'checked="checked"';
                      $on_main_road_val = '1';
                    } else {
                      $on_main_road_val = '0';
                      $on_main_road_attr = '';
                    }
                    if($data->residential_area=='1'){
                      $residential_area_attr = 'checked="checked"';
                      $residential_area_val = '1';
                    } else {
                      $residential_area_val = '0';
                      $residential_area_attr = '';
                    }
                  ?>
                  
                  <div class="form-group">
                    <input type="checkbox" value="1" name="lift_for_hall" <?php echo $lift_for_hall_attr; ?>>
                    <label for="lift_for_hall">Lift for Hall Available</label>
                    <input type='hidden' value='<?php echo $lift_for_hall_val; ?>' name='lift_for_hall'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="lift_for_dining_hall" <?php echo $lift_for_dining_hall_attr; ?>>
                    <label for="lift_for_dining_hall">Lift for Dining Hall Available</label>
                    <input type='hidden' value='<?php echo $lift_for_dining_hall_val; ?>' name='lift_for_dining_hall'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="hall_ac" <?php echo $hall_ac_attr; ?>>
                    <label for="hall_ac">Hall A/C Available</label>
                    <input type='hidden' value='<?php echo $hall_ac_val; ?>' name='hall_ac'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="dining_ac" <?php echo $dining_ac_attr; ?>>
                    <label for="dining_ac">Dining A/C Available</label>
                    <input type='hidden' value='<?php echo $dining_ac_val; ?>' name='dining_ac'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="common_ac" <?php echo $common_ac_attr; ?>>
                    <label for="common_ac">Common Area A/C Available</label>
                    <input type='hidden' value='<?php echo $common_ac_val; ?>' name='common_ac'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="generator_bckp" <?php echo $generator_bckp_attr; ?>>
                    <label for="generator_bckp">Generator Backup Available</label>
                    <input type='hidden' value='<?php echo $generator_bckp_val; ?>' name='generator_bckp'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="safe_avilable" <?php echo $safe_avilable_attr; ?>>
                    <label for="safe_avilable">Safe Available</label>
                    <input type='hidden' value='<?php echo $safe_avilable_val; ?>' name='safe_avilable'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="outdoor_catering" <?php echo $outdoor_catering_attr; ?>>
                    <label for="outdoor_catering">Outdoor Catering Allowed</label>
                    <input type='hidden' value='<?php echo $outdoor_catering_val; ?>' name='outdoor_catering'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="non_veg_cooking_allowed" <?php echo $non_veg_cooking_allowed_attr; ?>>
                    <label for="non_veg_cooking_allowed">Non-veg Cooking Allowed</label>
                    <input type='hidden' value='<?php echo $non_veg_cooking_allowed_val; ?>' name='non_veg_cooking_allowed'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="non_veg_service_allowed" <?php echo $non_veg_service_allowed_attr; ?>>
                    <label for="non_veg_service_allowed">Non-veg Service Allowed</label>
                    <input type='hidden' value='<?php echo $non_veg_service_allowed_val; ?>' name='non_veg_service_allowed'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="kitchen_vessels_available" <?php echo $kitchen_vessels_available_attr; ?>>
                    <label for="kitchen_vessels_available">Kitchen Vessels Available</label>
                    <input type='hidden' value='<?php echo $kitchen_vessels_available_val; ?>' name='kitchen_vessels_available'>
                  </div>
                </div>
                <div class="col-md-6">
                  <br><br><br><br>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="buffet_crockery_available" <?php echo $buffet_crockery_available_attr; ?>>
                    <label for="buffet_crockery_available">Buffet Crockery Available</label>
                    <input type='hidden' value='<?php echo $buffet_crockery_available_val; ?>' name='buffet_crockery_available'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="cooking_fuel_provided" <?php echo $cooking_fuel_provided_attr; ?>>
                    <label for="cooking_fuel_provided">Cooking Fuel Provided</label>
                    <input type='hidden' value='<?php echo $cooking_fuel_provided_val; ?>' name='cooking_fuel_provided'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="valet_parking" <?php echo $valet_parking_attr; ?>>
                    <label for="valet_parking">Valet Parking Available</label>
                    <input type='hidden' value='<?php echo $valet_parking_val; ?>' name='valet_parking'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="handicap_ramp" <?php echo $handicap_ramp_attr; ?>>
                    <label for="handicap_ramp">Handicap Ramp Available</label>
                    <input type='hidden' value='<?php echo $handicap_ramp_val; ?>' name='handicap_ramp'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="stage_available" <?php echo $stage_available_attr; ?>>
                    <label for="stage_available">Stage Available</label>
                    <input type='hidden' value='<?php echo $stage_available_val; ?>' name='stage_available'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="outside_vendor_allowed" <?php echo $outside_vendor_allowed_attr; ?>>
                    <label for="outside_vendor_allowed">Outside Vendor Allowed</label>
                    <input type='hidden' value='<?php echo $outside_vendor_allowed_val; ?>' name='outside_vendor_allowed'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="outside_entertainment_allowed" <?php echo $outside_entertainment_allowed_attr; ?>>
                    <label for="outside_entertainment_allowed">Outside Entertainment Allowed</label>
                    <input type='hidden' value='<?php echo $outside_entertainment_allowed_val; ?>' name='outside_entertainment_allowed'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="ritual" <?php echo $ritual_attr; ?>>
                    <label for="ritual">Ritual Allowed (Homam / Havan / Fire Ritual)</label>
                    <input type='hidden' value='<?php echo $ritual_val; ?>' name='ritual'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="on_main_road" <?php echo $on_main_road_attr; ?>>
                    <label for="on_main_road">Approach On Main Road</label>
                    <input type='hidden' value='<?php echo $on_main_road_val; ?>' name='on_main_road'>
                  </div>
                  <div class="form-group">
                    <input type="checkbox" value="1" name="residential_area" <?php echo $residential_area_attr; ?>>
                    <label for="residential_area">Approach Residential Area</label>
                    <input type='hidden' value='<?php echo $residential_area_val; ?>' name='residential_area'>
                  </div>
                </div>
              </div>
            </div> 
            <div class="box-footer center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>             
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->
  </section>
<!-- /.content -->
</div>
<script type="text/javascript">
  $('input[type="checkbox"]').change(function() {
    var check = $(this).prop('checked');
    console.log(check);
    if (check==true)
      $(this).closest(".form-group").find('input[type="hidden"]').val('1');
    else 
      $(this).closest(".form-group").find('input[type="hidden"]').val('0');
  });
  $(document).on('change','input[type="file"]' , function(e){
    var base = '<?php echo base_url(); ?>'; 
    var fileName = e.target.files[0].name;
    $(this).closest('.form-group').find('.hdn_image').val(base+"assets/uploads/wedding_halls/"+fileName);
  });
</script>