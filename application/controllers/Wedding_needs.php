<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wedding_needs extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
        parent::__construct();
        $this->load->model('Wedding_needs_model');
    }

    public function index() {
		$settings = get_setting();
		$header['title'] = $settings->title . " | Wedding Halls";
		$this->load->view('header', $header);
		$this->load->view('wedding_needs');
		$this->load->view('footer');  
	}

	public function halls() {
		$settings = get_setting();
		$header['title'] = $settings->title . " | Wedding Halls";
		$data['halls']  = $this->Wedding_needs_model->view_halls();
		$this->load->view('header', $header);
		$this->load->view('halls',$data);
		$this->load->view('footer');  
	}

	public function view_hall() {
		$hall_id = $this->input->get('hall');
		$settings = get_setting();
		$data['hall_details']  = $this->Wedding_needs_model->get_hall_details($hall_id);
		$data['gallery'] = [];
		if ($data['hall_details']) {
			for ($i=1; $i <= 10; $i++) {
				$var = "image_{$i}";
				$data['gallery'][] = $data['hall_details']->$var;
			}
			$data['gallery'] = array_filter($data['gallery']);
		}
		$header['title'] = $settings->title . " | Wedding Halls";
		$this->load->view('header', $header);
		$this->load->view('hall',$data);
		$this->load->view('footer');  
	}
}
