<?php 

class Wedding_needs_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}

	function view_halls() {
		$query = $this->db->get('wedding_hall');
		$result = $query->result();
		return $result;
	}

	function get_hall_details($id) {
		$this->db->where('id',$id);  
		$query=$this->db->get('wedding_hall');
		$result = $query->row();
		if($result) {
			return $result;
		} else {
			return '';
		}
	}

}