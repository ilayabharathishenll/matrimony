<?php if (!empty($hall_details)) { ?>
	<style>
		section.banner {
			background:url("<?php echo $hall_details->image_1; ?>");
			min-height: 440px;
			background-repeat: no-repeat;
			background-size: cover;
		}
	</style>
	<section class="container hall-detail module parallax parallax-21">
		<div class="hall-contact">
			<div class="hall-name">
				<h3><?php echo $hall_details->name; ?></h3>
				<p><?php echo $hall_details->address; ?>, <?php echo $hall_details->city; ?></p>
			</div>
			<a class="hall-check wed-find-btn" href="<?php echo 'mailto:'.$hall_details->contact_email.''?>">Contact</a>
		</div>
		<?php if (!empty($gallery)) { ?>
			<div class="clearfix hall-gallery">
				<div class="gallery-images">
					<?php foreach ($gallery as $image) { ?>
						<a class="col-md-3" href='<?php echo $image;?>' data-fancybox='images'>
							<img src='<?php echo $image;?>' class='img-responsive'>
						</a>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
		<div class="hall-features clearfix text-center">
			<div class="col-md-3 feature">
				<div class="fea-icon">
					<img class="img" src="<?php echo base_url(); ?>assets/images/hall_1.png">
					<img class="hover-img" src="<?php echo base_url(); ?>assets/images/hall.png">
				</div>
				<h5>Hall Capacity</h5>
				<h5><?php echo $hall_details->hall_seating; ?></h5>
			</div>
			<div class="col-md-3 feature">
				<div class="fea-icon">
					<img class="img" src="<?php echo base_url(); ?>assets/images/dining_1.png">
					<img class="hover-img" src="<?php echo base_url(); ?>assets/images/dining.png">
				</div>
				<h5>Dining Capacity</h5>
				<h5><?php echo $hall_details->dining_seating; ?></h5>
			</div>
			<div class="col-md-3 feature">
				<div class="fea-icon">
					<img class="img" src="<?php echo base_url(); ?>assets/images/parking_1.png">
					<img class="hover-img" src="<?php echo base_url(); ?>assets/images/parking.png">
				</div>
				<h5>Car <?php echo $hall_details->car_parking; ?></h5>
				<h5>Bike <?php echo $hall_details->bike_parking; ?></h5>
			</div>
			<div class="col-md-3 feature">
				<div class="fea-icon">
					<img class="img" src="<?php echo base_url(); ?>assets/images/rooms_1.png">
					<img class="hover-img" src="<?php echo base_url(); ?>assets/images/rooms.png">
				</div>
				<h5>Rooms</h5>
				<h5>A/C: <?php echo $hall_details->no_of_ac_rooms; ?>, Non-A/C: <?php echo ($hall_details->no_of_rooms - $hall_details->no_of_ac_rooms); ?></h5>
			</div>
		</div>
		<div class="hall-specs clearfix">
			<div class="col-md-6 spec-section">
				<div class="spec-detail">
					<img src="<?php echo base_url(); ?>assets/images/icon-about.png">
					<h3>About <?php echo $hall_details->name; ?></h3>
					<div class="spec-desc">
						<p><?php echo $hall_details->description; ?></p>
					</div>
				</div>
				<div class="spec-detail">
					<img src="<?php echo base_url(); ?>assets/images/icon-hall.png">
					<h3>Hall Capacity</h3>
					<div class="spec-desc">
						<table><tr><td>Hall Seating</td><td>:</td><td><?php echo $hall_details->hall_seating; ?></td></tr></table>
						<table><tr><td>Hall Floating</td><td>:</td><td><?php echo $hall_details->hall_floating; ?></td></tr></table>
					</div>
				</div>
				<div class="spec-detail">
					<img src="<?php echo base_url(); ?>assets/images/icon-dining.png">
					<h3>Dining Capacity</h3>
					<div class="spec-desc">
						<table><tr><td>Dining Seating</td><td>:</td><td><?php echo $hall_details->dining_seating; ?></td></tr></table>
						<table><tr><td>Dining Hall Level</td><td>:</td><td><?php echo $hall_details->dining_hall_floor; ?></td></tr></table>
						<table><tr><td>Lift Access to Dining Hall</td><td>:</td><td><?php if ($hall_details->lift_for_dining_hall) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
					</div>
				</div>
				<div class="spec-detail">
					<img src="<?php echo base_url(); ?>assets/images/icon-ac.png">
					<h3>Airconditioning</h3>
					<div class="spec-desc">
						<table><tr><td>Hall Ac</td><td>:</td><td><?php if ($hall_details->hall_ac) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
						<table><tr><td>Dining Ac</td><td>:</td><td><?php if ($hall_details->dining_ac) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
						<table><tr><td>Common Area Ac</td><td>:</td><td><?php if ($hall_details->common_ac) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
						<table><tr><td>Generator Backup</td><td>:</td><td><?php if ($hall_details->generator_bckp) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
					</div>
				</div>
				<div class="spec-detail">
					<img src="<?php echo base_url(); ?>assets/images/icon-rooms.png">
					<h3>Rooms</h3>
					<div class="spec-desc">
						<table><tr><td>Total Rooms</td><td>:</td><td><?php echo $hall_details->no_of_rooms; ?></td></tr></table>
						<table><tr><td>Total AC Rooms</td><td>:</td><td><?php echo $hall_details->no_of_ac_rooms; ?></td></tr></table>
						<table><tr><td>Safe Available</td><td>:</td><td><?php if ($hall_details->safe_avilable) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
					</div>
				</div>
			</div>
			<div class="col-md-6 spec-section">
				<div class="spec-detail">
					<img src="<?php echo base_url(); ?>assets/images/icon-food-services.png">
					<h3>Food & Services</h3>
					<div class="spec-desc">
						<table><tr><td>Outdoor Catering Allowed</td><td>:</td><td><?php if ($hall_details->outdoor_catering) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
						<table><tr><td>Non-Veg Cooking Allowed</td><td>:</td><td><?php if ($hall_details->non_veg_cooking_allowed) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
						<table><tr><td>Non-Veg Service Allowed</td><td>:</td><td><?php if ($hall_details->non_veg_service_allowed) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
						<table><tr><td>Kitchen Vessels For Cooking & Serving Available</td><td>:</td><td><?php if ($hall_details->kitchen_vessels_available) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
						<table><tr><td>Buffet Crockery Available</td><td>:</td><td><?php if ($hall_details->buffet_crockery_available) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
						<table><tr><td>Cooking Fuel Provided</td><td>:</td><td><?php if ($hall_details->cooking_fuel_provided) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
					</div>
				</div>
				<div class="spec-detail">
					<img src="<?php echo base_url(); ?>assets/images/icon-parking.png">
					<h3>Parking</h3>
					<div class="spec-desc">
						<table><tr><td>Car Parking</td><td>:</td><td><?php echo $hall_details->car_parking; ?></td></tr></table>
						<table><tr><td>Bike Parking</td><td>:</td><td><?php echo $hall_details->bike_parking; ?></td></tr></table>
						<table><tr><td>Valet Parking Available</td><td>:</td><td><?php if ($hall_details->valet_parking) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
					</div>
				</div>
				<div class="spec-detail">
					<img src="<?php echo base_url(); ?>assets/images/icon-hall-dimensions.png">
					<h3>Hall Dimensions & Access</h3>
					<div class="spec-desc">
						<table><tr><td>Hall Square Ft</td><td>:</td><td><?php echo $hall_details->hall_sq_ft; ?></td></tr></table>
						<table><tr><td>Hall Level</td><td>:</td><td><?php echo $hall_details->hall_floor; ?></td></tr></table>
						<table><tr><td>Lift Access to Hall</td><td>:</td><td><?php if ($hall_details->lift_for_hall) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
						<table><tr><td>Handicap Ramp Available</td><td>:</td><td><?php if ($hall_details->handicap_ramp) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
						<table><tr><td>Stage Available</td><td>:</td><td><?php if ($hall_details->stage_available) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
					</div>
				</div>
				<div class="spec-detail">
					<img src="<?php echo base_url(); ?>assets/images/icon-decor.png">
					<h3>Decor & Ritual</h3>
					<div class="spec-desc">
						<table><tr><td>Outside Vendor Allowed</td><td>:</td><td><?php if ($hall_details->outside_vendor_allowed) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
						<table><tr><td>Outside Entertainment Allowed</td><td>:</td><td><?php if ($hall_details->outside_entertainment_allowed) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
						<table><tr><td>Homam / Havan / Fire Ritual</td><td>:</td><td><?php if ($hall_details->ritual) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
					</div>
				</div>
				<div class="spec-detail">
					<img src="<?php echo base_url(); ?>assets/images/icon-approach.png">
					<h3>Approach</h3>
					<div class="spec-desc">
						<table><tr><td>On Main Road</td><td>:</td><td><?php if ($hall_details->on_main_road) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
						<table><tr><td>Residential Area</td><td>:</td><td><?php if ($hall_details->residential_area) { echo ' YES '; } else { echo ' NO '; } ?></td></tr></table>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php } ?>