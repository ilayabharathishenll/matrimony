<div class="container-fulid">
<?php
$url="http://localhost/matrimony/";
?>
<style>
.login.top {
    margin-top: 50px;
    margin-bottom: 50px;
}
/*.container {
    width: 950px !important;
}*/
.p10px {
    padding: 10px;
}

.joinNow {
}
.gray, .gray a, a.gray {
    color: #666;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
}


 h1 {
    color: #f60;
    display: block;
    font-size: 20px;
    font-weight: normal;
}
.bdrBd {
    border-bottom: 1px dashed #ccc;
}
.p5px10px {
    padding: 5px 10px;
}
.mb15px {
    margin-bottom: 15px;
}
.mr20px {
    margin-right: 20px;
}

	.bdr, .bdrT, .bdrR, .bdrB, .bdrL {
    border-color: #ddd;
}
.bdr {
    border: 1px solid;
	border-color: #ddd !important;
}

.p10px {
    padding: 10px;
}


table {
    border-collapse: collapse;
    border-spacing: 0;
}


.p5px10px {
    padding: 5px 10px;
}


.xsmall {
    font-size: 0.8em;
}



.joinNow input, select {
    font-size: 12px;
}
.vam {
    vertical-align: middle;
}
input, select, textarea {
    font-family: Arial,Helvetica,sans-serif;
    font-size: 1em;
}



.gray, .gray a, a.gray {
    color: #666;
}
.uu a {
    text-decoration: underline;
}
a {
    color: #333;
    text-decoration: none;
}

.join_bul {
    background: rgba(0, 0, 0, 0) url("../../assets/login/join_bul.gif") no-repeat scroll left 8px;
    font: 12px/1.5em Arial,Helvetica,sans-serif;
    margin: 0;
    padding: 5px 10px 5px 24px;
}

td {
	background: none !important;
    background-color: none;
    border: none;
    padding: 20px 15px;
}
td, th {
    padding: 0;
}



.pKind {
    color: #666;
    font-size: 12px;
    margin: 15px 0 10px;
}
.jnc{ display:none; }
 .mediaCheck{
    padding-left: 0px;
    padding-right: 0px;
 }
 .vam{float:none;}
 .wid_cond{
 padding-left: 0;
    padding-right: 0;
 }
 .input {
    width: 100%;
}
.pull-left {
    font-size: 12px;
    line-height: 28px;
	text-decoration: none !important;
}
@media (min-width: 640px) {
    .mediaCheck{
        border-right:1px solid #f6563a; padding-right:40px;
    }
	.jnc{ display:block; }	
	.vam{float:right;}
	 .wid_cond{
 padding-left: 15px;
    padding-right: 15px;
 }
}
</style>

<div class="container-fulid banner-section top-sect">
	   
	   </div>


<div class="container p10px login top">
	<div class="row" style="border:1px solid #f6563a; border-top:3px solid #6EB551; padding:25px; background:url(../../assets/login/bg-patten.jpg) repeat top center;">
    	<div class="col-md-6 col-sm-12 col-sx-12 mediaCheck" >
    <h1 class="p5px10px mb15px bdrBd addedfrm" id="addedfrm">Login / Register</h1>
    <p class="pKind">Kindly provide your Login ID &amp; Password to enjoy our uninterupted services</p><br>
	
	<div class="bdr">
	<p style="background:#FFF url(../../assets/login/fld_g1.gif) repeat-x bottom;" class="fs18 p10px">Member Login</p>
	<div class="p10px">
	<form method="post" action='<?php echo base_url(); ?>home/login' id="login_form">
	
	<div class="row">
    <div class="col-md-4 col-sm-12 col-sx-12 p5px10px">Login (Email ID):</div>	
    <div class="col-md-5 col-sm-12 col-sx-12 p5px10px">
    <input type="text" value="" name="email" placeholder="Email/Matrimonyid" class="validate[required,custom[email]] input w70" style="text-transform: lowercase;" data-parsley-trigger="change" required>
    </div>
    <div class="col-md-3 col-sm-12 col-sx-12 p5px10px"></div>
    </div>	
    
    
    <div class="row">
    <div class="col-md-4 col-sm-12 col-sx-12 p5px10px">Password:</div>	
    <div class="col-md-5 col-sm-12 col-sx-12 p5px10px">
    <input type="password" value="" name="password" style="padding:3px;" class="validate[required] input w70" data-parsley-trigger="change" required>&nbsp;
        </div>
    <div class="col-md-3 col-sm-12 col-sx-12 p5px10px xsmall"><input id="remember_me" type="checkbox" name="remember" value="1"> Remember Me</div>
    </div>	
   
    <div class="row">
    <div class="col-md-6 col-sm-12 col-sx-12 p5px10px uu red2 xsmall"><input type="image" src="../../assets/login/ml_loginBtn.gif" class="vam"> &nbsp;</div>	
    <div class="col-md-6 col-sm-12 col-sx-12 p5px10px"><a class="pull-left" data-target="#forgot" data-toggle="modal">Forgot Password ?</a></div>
    </div>
	</form>
	</div>
	</div>

    
    </div>
    <div class="brred"></div>
        <div class="col-md-6 col-sm-12 col-sx-12 wid_cond">
        
        
       <div class="row gray joinNow">
       <div class="col-md-12 col-sm-12 col-sx-12 wid_cond">
       
       
	<h1 class="p5px10px mb15px bdrBd " id="addedfrm">Not Yet a Member? <a class="jnc" style="float:right" title="join now" href="<?php echo base_url(); ?>">Join Now</a></h1>
<div class="fs15px" style="color:#65a900;">
<p class="join_bul">1. View Profiles (Free)</p>
<p class="join_bul">2. Contact Profiles</p>
<p class="join_bul">3. Get Match Alerts</p>
<p class="join_bul">4. Weekly e-mail alerts</p>
<p class="join_bul">5. Get Match Alerts</p><br>
<p class="ac fs20"><a href="<?php echo base_url(); ?>"><img src="../../assets/login/registerNow.gif" class="vam1"></a></p>
</div>

</div>
       </div>
      
        </div>
    
    
    
	</div>
	</div>
    </div>