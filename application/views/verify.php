<?php  $my_matr_id = $this->session->userdata('logged_in');

/*var_dump($my_matr_id);
die();*/
       $email=$my_matr_id->email;?>

		  
		   <!-- MODAL FOR LOGIN START -->
    <div class="modal fade wed-add-modal web-add-modal-custom" id="login" role="dialog">
      <div class="modal-dialog wed-add-modal-dialogue">
        <div class="modal-content wed-add-modal-content  login_modal_content">
          
          <div class="modal_close">
            <button class="modal_close_btn" data-dismiss="modal">
              <i class="fa fa-times" aria-hidden="true"></i>
            </button>
          </div>

          <div class="login_modal">
            <div class="login_modal_head">
              <span class="login_modal_img"><img src="<?php echo base_url(); ?>assets/images/login.png"></span>
              Member Login 
            </div>             
            <form method="post" action='<?php echo base_url(); ?>home/login' id="login_form">
              <input class="wed-navbar-input" type="text" placeholder="Email/Matrimonyid" name="email" data-parsley-trigger="change" required>
              <input class="wed-navbar-input" type="password" placeholder="password" name="password" data-parsley-trigger="change" required>
              <div class="login_modal_remember">
                  <input id="remember_me" type="checkbox" name="remember" value="1">
                  <!-- <input id="remember_me" type="checkbox" value="check1" data-parsley-mincheck="1" data-parsley-trigger="change" required> -->
                  <label for="remember_me">Remember me</label>
              </div>
              <div class="modal_login_button">
                <button class="wed-login main-button" id="login_user" type="submit">Login</button>
                 <p data-toggle="modal" data-target="#forgot" class="forgot">Forgot Password</p>
              </div> 
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- MODAL FOR LOGIN END -->

    <!-- MODAL FOR LOGIN ERROR START -->
    <div class="modal fade wed-add-modal web-add-modal-custom" id="loginError" role="dialog">
      <div class="modal-dialog wed-add-modal-dialogue">
        <div class="modal-content wed-add-modal-content  login_modal_content">
          
          <div class="modal_close">
            <button class="modal_close_btn" data-dismiss="modal">
              <i class="fa fa-times" aria-hidden="true"></i>
            </button>
          </div>

          <div class="login_modal">
            <div class="login_modal_head">
              <span class="login_modal_img"><img src="<?php echo base_url(); ?>assets/images/login.png"></span>
              Login Error
            </div>             
            <div class="login_modal_head">
              <?php echo $logn_err;?>
              </div>
              <div class="modal_login_button">
                <button class="wed-login" data-dismiss="modal">Close</button>
              </div> 
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- MODAL FOR LOGIN ERROR END -->

    <!-- MODAL FOR FORGOT PASSWORD START -->
    <div class="modal fade wed-add-modal" id="forgot" role="dialog">
      <div class="modal-dialog wed-add-modal-dialogue">
        <div class="modal-content login_modal_content">             
          <form method="post" action="" id="frgt_psw_form" data-parsley-validate="true" class="validate">
            <div class="modal-body  wed-add-modal-body">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4>Forgot Password</h4>
              <p>Please enter your E-mail ID. We will send you a link to reset your password. </p>  
               <div id="frgt_psw_msg" class="renew_pass" style="color:#fff;"></div>         
              <input type="email" id="email" name="email" class="wed-forgot-input" placeholder="E-mail" required=""
               data-parsley-required-message="Please insert email."
               data-parsley-errors-container="#frgt_psw_msg">

              <input class="wed-forgot-submit" type="button" value="Send"  id="frgt_psw"> 
              <div class="view_loader"></div>          
              <div class="clearfix"></div>
            </div>
          </form>
        </div>
      </div>
    </div>

        <div class="modal fade wed-add-modal" id="confirm" role="dialog">
            <div class="modal-dialog wed-add-modal-dialogue">
              <div class="modal-content wed-add-modal-content">
              <div class="modal-body  wed-add-modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>Create New Password</h4>
                <p>Please Enter your Password </p>
                <input class="wed-forgot-input1" placeholder="Enter New Password"><br>
                <input class="wed-forgot-input1" placeholder="Confirm New Password">
                <input class="wed-forgot-submit" type="button" value="Submit">
                <div class="clearfix"></div>
              </div>
              </div>
            </div>
            </div>
		  
<div class="row bhk">
	<marquee>இலவச தொடர்பு மையம்,முழு ஜாதக விபரத்துடன் புகைப்படம் தொலைபேசி எண்ணை விளம்பரம் செய்யும் ஒரே திருமண விளமபர இணையத்தளம், திருமணம் முடியும் வரை விளம்பரம் செய்ய ருபாய் 750 மட்டுமே.வேறு எந்த கட்டணமோ,கமிஷனோ இல்லை, இந்த வெளிப்படையான சேவையை உங்கள் உறவினர்களுக்கும்,நண்பர்களுக்கும் எடுத்து சொல்லி நமது சமுதாய வரன்களை அதிகமாக்கி விரைவாக திருமணம் முடியுங்கள் ,வாழ்க வளமுடன் .</marquee>
	</div>
	  <!-- TOP-BANNER -->
    <div class="wed-wrapper">
    <div class="wed-verify-banner">
      <div class="container container-custom">
        <div class="wed-congrats">
          <h1>Congratulations !</h1>
          <p>You have Successfully registered<br>
              with Thirumanaveedu Matrimony</p>
              <h5>ID : <?php echo $my_matr_id->matrimony_id;?></h5>
        </div>
      </div>
    </div>
    <div class="wed-verify-detail">
        <div class="container container-custom">
          <div class="wed-verify-inner">
            <p>A  6 - Digit Confirmation code has been sent to your email address<strong><?php echo $email;?></strong><span><img src="<?php echo base_url();?>assets/img/verify-edit.png"></span></p>
            <div class="wed-verify-code">
              <form action="<?php echo base_url();?>Verify/check_otp" method="post">
              <input class="wed-verify-input" type="text" placeholder="Enter the code" name="otp">
              <button class="wed-verify-btn" type="submit">Verify</button>
              <span style="color:red;"><?php if(isset($error)) { echo $error; } ?></span>
            </form>
              <p>Didn’t receive code yet?<br>
                <a href="<?php echo base_url(); ?>Verify/resend_otp"><strong>Resend Pin</strong></a>
              </p>
                <hr>
               <!--  <div class="wed-custom-check">
                    <input id="check1" type="checkbox" name="check" value="check1">
                    <label for="check1">Keep me Logged In ( Recommended )</label>
                    <div class="clearfix"></div>
                </div>
                <button class="wed-skip-btn">Skip</button> -->
            </div>
          </div>
        </div>
    </div>


</div>
    
</body></html>
