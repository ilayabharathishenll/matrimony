<?php  ?>
<style>
  section.banner {
    background:url("<?php echo base_url(); ?>assets/images/needs_banner.jpg");
    min-height: 440px;
    background-repeat: no-repeat;
    background-size: cover;
  }
</style>
<section class="banner">
</section>
<section class="container module parallax parallax-21">
	<div class="col-md-4 hall-list text-center">
		<img src="<?php echo base_url(); ?>assets/images/Wedding-Halls.JPG" alt="<?php echo $hall->name; ?>" title="<?php echo $hall->name; ?>">
		<h3>Wedding Halls</h3>
		<a class="hall-check wed-find-btn" href="<?php echo base_url(); ?>wedding_needs/halls">Check Halls</a>
		<hr>
	</div>
	<div class="col-md-4 hall-list text-center">
		<img src="<?php echo base_url(); ?>assets/images/catering.jpg" alt="<?php echo $hall->name; ?>" title="<?php echo $hall->name; ?>">
		<h3>Catering Services</h3>
		<a class="hall-check wed-find-btn" href="javascript:;">Check Services</a>
		<hr>
	</div>
	<div class="col-md-4 hall-list text-center">
		<img src="<?php echo base_url(); ?>assets/images/wedding-decorators.jpg" alt="<?php echo $hall->name; ?>" title="<?php echo $hall->name; ?>">
		<h3>Decorators</h3>
		<a class="hall-check wed-find-btn" href="javascript:;">Check Decorators</a>
		<hr>
	</div>
	<div class="col-md-4 hall-list text-center">
		<img src="<?php echo base_url(); ?>assets/images/wedding-bands.jpg" alt="<?php echo $hall->name; ?>" title="<?php echo $hall->name; ?>">
		<h3>Orchestras</h3>
		<a class="hall-check wed-find-btn" href="javascript:;">Check Orchestras</a>
		<hr>
	</div>
	<div class="col-md-4 hall-list text-center">
		<img src="<?php echo base_url(); ?>assets/images/wedding-photo.jpg" alt="<?php echo $hall->name; ?>" title="<?php echo $hall->name; ?>">
		<h3>Photographers</h3>
		<a class="hall-check wed-find-btn" href="javascript:;">Check Photographers</a>
		<hr>
	</div>
</section>


