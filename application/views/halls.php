<?php  ?>
<section class="container module parallax parallax-21">
	<?php if (!empty($halls)) {
		foreach($halls as $hall) { ?>
			<div class="col-md-4 hall-list text-center">
				<img src="<?php echo $hall->image_1; ?>" alt="<?php echo $hall->name; ?>" title="<?php echo $hall->name; ?>">
				<h3><?php echo $hall->name; ?></h3>
				<h5><?php echo $hall->city; ?></h5>
				<a class="hall-check wed-find-btn" href="<?php echo base_url(); ?>wedding_needs/view_hall?hall=<?php echo $hall->id; ?>">Check Now</a>
				<hr>
			</div>
		<?php }
	} ?>
</section>